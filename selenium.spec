%global _empty_manifest_terminate_build 0
%global common_desc \
	Selenium Client Driver Introduction Python language bindings for Selenium \
	WebDriver.The selenium package is used to automate web browser interaction from \
	Python.
Name:           python-selenium
Version:        4.18.1
Release:        1
Summary:        Python bindings for Selenium
License:        Apache-2.0
URL:            https://github.com/SeleniumHQ/selenium/
Source0:        https://files.pythonhosted.org/packages/16/fd/a0ef793383077dd6296e4637acc82d1e9893e9a49a47f56e96996909e427/selenium-4.18.1.tar.gz
%description
%{common_desc}

%package -n python3-selenium
Summary:        Python bindings for Selenium
Provides:       python-selenium
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-urllib3
BuildRequires:  python3-certifi
BuildRequires:  python3-trio-websocket
Requires:       python3-urllib3
%description -n python3-selenium
%{common_desc}

%package help
Summary:        Python bindings for Selenium
Provides:       python3-selenium-doc
%description help
%{common_desc}

%prep
%autosetup -n selenium-%{version}
sed -i 's/~=/>=/g' setup.py
rm -rf selenium/webdriver/firefox/x86/x_ignore_nofocus.so
rm -rf selenium/webdriver/firefox/amd64/x_ignore_nofocus.so

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-selenium -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Mar 14 2024 wangqiang <wangqiang1@kylinos.cn> - 4.18.1-1
- Update package to version 4.18.1

* Thu Dec 28 2023 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 4.16.0-1
- Upgrade to version 4.16.0

* Mon Nov 6 2023 Dongxing Wang <dxwangk@isoftstone.com> - 4.15.2-1
- Upgrade to version 4.15.2

* Fri Aug 13 2021 liksh <liks11@chinaunicom.cn> - 3.141.0-3
- delete x_ignore_nofocus.so

* Wed Aug 11 2021 huangtianhua <huangtianhua@huawei.com> - 3.141.0-2
- Adds python3-urllib3 as requires

* Tue Jul 20 2021 OpenStack_SIG <openstack@openeuler.org> - 3.141.0-1
- Package Spec generate
